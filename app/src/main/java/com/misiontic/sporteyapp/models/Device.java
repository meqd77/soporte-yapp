package com.misiontic.sporteyapp.models;

public class Device {

    private String description, brand, usage;

    public Device(String description, String brand, String usage) {
        this.description = description;
        this.brand = brand;
        this.usage = usage;
    }

    public Device() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "Device{" +
                "description='" + description + '\'' +
                ", brand='" + brand + '\'' +
                ", usage='" + usage + '\'' +
                '}';
    }
}
