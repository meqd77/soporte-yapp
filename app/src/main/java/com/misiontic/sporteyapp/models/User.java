package com.misiontic.sporteyapp.models;

public class User {

    private String name,email,pass,direct,tel;

    public User(String name, String email, String pass, String direct, String tel) {
        this.name = name;
        this.email = email;
        this.pass = pass;
        this.direct = direct;
        this.tel = tel;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", pass='" + pass + '\'' +
                ", direct='" + direct + '\'' +
                ", tel='" + tel + '\'' +
                '}';
    }
}
