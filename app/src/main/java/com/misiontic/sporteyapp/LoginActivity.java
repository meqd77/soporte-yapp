package com.misiontic.sporteyapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.misiontic.sporteyapp.persistencia.DbUsuarios;

public class LoginActivity extends AppCompatActivity {

    private TextView goToRegistro, goToForgotPass;
    private EditText emailLogin, passwordLogin;
    private Button buttonlogin, btnRegis; // Declaramos de forma global nuestra variable Button
    private DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        //getSupportActionBar().hide(); // Eliminamos la barrar superior

        emailLogin = findViewById(R.id.emailLogin);
        passwordLogin = findViewById(R.id.passwordLogin);
        Button buttonlogin = findViewById(R.id.buttonLogin);
        goToForgotPass = findViewById(R.id.forgotPassHome);

        if(BuildConfig.DEBUG){
            emailLogin.setText("mae");
            passwordLogin.setText("123");
        }
        DB = new DbUsuarios(this);
        goToForgotPass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent goToFP= new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(goToFP);
            }
        });

        // Conetext = La clase en la que estamos ubicados ahora mismo
        DB = new DbUsuarios(this);

        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = emailLogin.getText().toString(); // Tomar los datos que agregó el usuario
                String pass = passwordLogin.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass)) {
                    Toast.makeText(LoginActivity.this, "Todos los espacios son requeridos", Toast.LENGTH_SHORT).show();
                }else {
                    Boolean checkuserpass = DB.checkContrasena(user, pass);
                    if (checkuserpass == true) {
                        Toast.makeText(LoginActivity.this, "Login correcto", Toast.LENGTH_SHORT).show();
                        Intent goToHome = new Intent(getApplicationContext(),HomeActivity.class);
                        startActivity(goToHome);
                    }
                    else {
                        Toast.makeText(LoginActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }

            }

        });

        this.goToRegistro = findViewById(R.id.txtRegistrarse);
        this.goToRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToRegistroInt = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(goToRegistroInt);
            }
        });

    }
}