package com.misiontic.sporteyapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.misiontic.sporteyapp.helpers.Validator;
import com.misiontic.sporteyapp.persistencia.DbUsuarios;

public class RegisterActivity extends AppCompatActivity {

    EditText txtNombre, email, password, adress, mobile;
    Button buttonregister, volverLogin; // Variable Global
    DbUsuarios DB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtNombre = findViewById(R.id.txtNombre);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        adress = findViewById(R.id.adress);
        mobile = findViewById(R.id.mobile);
        buttonregister = findViewById(R.id.buttonregister);
        volverLogin = findViewById(R.id.volverLogin);
        DB = new DbUsuarios(this);

        buttonregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString(); //
                String correo = email.getText().toString();
                String contrasena = password.getText().toString();
                String direccion = adress.getText().toString();
                String celular = mobile.getText().toString();

                Validator validator = new Validator();

                if (TextUtils.isEmpty(nombre) || TextUtils.isEmpty(correo) || TextUtils.isEmpty(contrasena) || TextUtils.isEmpty(direccion) || TextUtils.isEmpty(celular)) {
                    Toast.makeText(RegisterActivity.this, "Se requiere llenar los campos", Toast.LENGTH_SHORT).show();
                }else{

                    if(validator.isMobileNumber(celular)){
                        boolean insert = DB.insertarUsuario(nombre,correo,contrasena,direccion,celular);
                        if(insert){
                            Toast.makeText(RegisterActivity.this, "Usuario registrado", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(RegisterActivity.this, "El usuario no se pudo registrar", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Número de celular no valido", Toast.LENGTH_SHORT).show();
                    }

                }
            }

        });
        volverLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new  Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);

            }
        });



    }
}