package com.misiontic.sporteyapp.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

public class DbUsuarios extends DbHelper{

    Context context; // Variable global

    // Constructor
    public DbUsuarios(@Nullable Context context) {
        // SUPER: Llama al contructor de la clase padre
        super(context);
        this.context = context;
    }

    // Long: Es de tipo entero de mayor tamaño


    // NEW
    public Boolean insertarUsuario(String technicalmessage, String email, String password, String adress, String mobile){

        DbHelper dbHelper = new DbHelper(context); // Intancia del objeto DbHleper = nuestra base de datos
        SQLiteDatabase db = dbHelper.getWritableDatabase(); // Agregamos los datos

        ContentValues values = new ContentValues(); // Instancia del objeto values
        values.put("nombre", technicalmessage);
        values.put("email", email);
        values.put("password", password);
        values.put("adress", adress);
        values.put("mobile", mobile);


        long result = db.insert(TABLE_USERS, null, values);

        if (result == -1){
            return false;
        }
        else {
            return true;
        }

    }

    public Boolean checkNomUsuario(String emailLogin){ //esta es ka qye esta llegando null
        DbHelper dbHelper = new DbHelper(context); // Instancia del objeto DbHelper
        SQLiteDatabase db = dbHelper.getWritableDatabase(); //
        // Colección de filas que son aleatorias
        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE email =?", new String[] {emailLogin});
        if(cursor.getCount()>0) {
            return true;
        }else {
            return false;
        }
    }

    public Boolean checkContrasena(String emailLogin, String passwordLogin){

        DbHelper dbHelper = new DbHelper(context); // Instaciamos nuestra conexión
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM usuarios WHERE email =? and password=?", new String[] {emailLogin, passwordLogin});

        if(cursor.getCount()>0){
            return true;
        }else {
            return false;
        }
    }
}

