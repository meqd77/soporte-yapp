package com.misiontic.sporteyapp.helpers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ValidatorTest {

    private Validator validator;

    @Before
    public void setUp() throws Exception {

        this.validator = new Validator();

    }

    @Test
    public void isMobileNumber() {

        assertEquals(true,validator.isMobileNumber("3124567891"));

    }
}